package nl.robfaber.serial;

import gnu.io.CommPortIdentifier;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.slf4j.MarkerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Slf4j
public class SerialDiscovery {

  public static SerialDevice autodiscoverPort(String deviceName, String matchString) {
    val marker = MarkerFactory.getMarker(deviceName);
    log.info(marker, "Trying to autodiscover port for " + deviceName);
    String deviceId = null;
    Process p;
    try {
      String[] cmd = {
          "/bin/sh",
          "-c",
          "ls -l /dev/serial/by-id | grep " + matchString
      };
      p = Runtime.getRuntime().exec(cmd);
      p.waitFor();

      BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

      StringBuffer sb = new StringBuffer();
      String line;
      while ((line = reader.readLine()) != null) {
        sb.append(line + "\n");
      }

      String output = sb.toString();
      log.info(output);

      if (output != null) {
        if (output.contains("ttyUSB0")) {
          deviceId = "/dev/ttyUSB0";
        } else if (output.contains("ttyUSB1")) {
          deviceId = "/dev/ttyUSB1";
        } else if (output.contains("ttyUSB2")) {
          deviceId = "/dev/ttyUSB2";
        } else if (output.contains("ttyUSB3")) {
          deviceId = "/dev/ttyUSB3";
        } else if (output.contains("ttyUSB4")) {
          deviceId = "/dev/ttyUSB4";
        } else if (output.contains("ttyUSB5")) {
          deviceId = "/dev/ttyUSB5";
        }
      }
    } catch (Exception e) {
      log.error(marker, "Failed to autodiscover", e);
    }
    if (deviceId != null) {
      return SerialDevice.builder()
          .deviceId(deviceId)
          .name(deviceName)
          .build();
    }
    return null;
  }

  public static List<String> listPorts(String prefix) {
    List<String> ports = new ArrayList<>();
    Enumeration e = CommPortIdentifier.getPortIdentifiers();
    while (e.hasMoreElements()) {
      CommPortIdentifier port = (CommPortIdentifier) e.nextElement();
      if (port.getName().startsWith(prefix)) {
        ports.add(port.getName());
      }
    }
    return ports;
  }

//  public static void main(String[] args) {
//    listPorts("/dev/tty.usb").get(0);
//  }
}
