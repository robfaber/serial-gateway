package nl.robfaber.serial;

public interface SerialPortMessageListener {
  void serialReceived(String message);
}
