package nl.robfaber.serial;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPort;
import gnu.io.UnsupportedCommOperationException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.slf4j.MarkerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Slf4j
public class SerialPortStream {
  private String name;
  private int speed;

  private volatile gnu.io.SerialPort port;

  public static SerialPortStream open(SerialDevice device, int baudrate) throws SerialException {
    val marker = MarkerFactory.getMarker(device.getName());
    log.info(marker, "Setting up " + device.getDeviceId());
    SerialPortStream serialPort = new SerialPortStream(device.getDeviceId(), baudrate);
    serialPort.connect();
    log.info(marker, "Done");
    return serialPort;
  }

  protected SerialPortStream(String portName, int speed) {
    this.name = portName;
    this.speed = speed;
  }

  public void connect() throws SerialException {
    int parity = 0;
    int dataBits = 8;
    int stopBits = 1;
    log.info("Connecting to port:" + name + ", speed:" + speed + ", dataBits:" + dataBits + ", stopBits:" + stopBits + ", parity:" + parity);

    try {
      CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(name);
      if (portIdentifier.isCurrentlyOwned()) {
        log.info("Error: Port is currently in use");
      } else {
        SerialPort commPort = portIdentifier.open(getClass().getName(), 2000);

        if (commPort != null) {
          port = commPort;
          port.setSerialPortParams(speed, dataBits, stopBits, parity);
        } else {
          log.error("Error: Not a serial port");
          throw new RuntimeException("Failed to initialize port");
        }
      }
    } catch (NoSuchPortException e) {
      port = null;
      throw new SerialException("No such serial port", e);
    } catch (PortInUseException e) {
      port = null;
      throw new SerialException("Serial port in use", e);
    } catch (UnsupportedCommOperationException e) {
      port = null;
      throw new SerialException("Unsupported serial communication", e);
    }
  }

  public InputStream getInputStream() throws IOException {
    if (port == null) {
      return null;
    }
    return port.getInputStream();
  }

  public OutputStream getOutputStream() throws IOException {
    if (port == null) {
      return null;
    }
    return port.getOutputStream();
  }

  public void disconnect() {
    this.port.close();
    this.port = null;
  }
}
