package nl.robfaber.serial;

public class SerialException extends Exception {
  public SerialException() {
  }

  public SerialException(String message) {
    super(message);
  }

  public SerialException(String message, Throwable cause) {
    super(message, cause);
  }
}
