package nl.robfaber.serial;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class SerialDevice {
  private String name;
  private String deviceId;

  private SerialDevice() {
  }

  public static SerialDevice of(String name, String deviceId) {
    return new SerialDevice(name, deviceId);
  }
}
