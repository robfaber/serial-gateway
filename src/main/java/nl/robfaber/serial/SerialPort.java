package nl.robfaber.serial;

import gnu.io.CommPortIdentifier;
import gnu.io.NoSuchPortException;
import gnu.io.PortInUseException;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import gnu.io.UnsupportedCommOperationException;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.slf4j.MarkerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TooManyListenersException;

@Slf4j
public class SerialPort implements SerialPortEventListener {
  private String name;
  private int speed = 115200;

  private SerialPortMessageListener listener;
  private volatile gnu.io.SerialPort port;

  public static SerialPort open(SerialDevice device, Integer baudrate, SerialPortMessageListener listener) throws SerialException {
    val marker = MarkerFactory.getMarker(device.getName());
    log.info(marker, "Setting up " + device.getDeviceId());
    SerialPort serialPort = new SerialPort(device.getDeviceId(), baudrate, listener);
    serialPort.connect();
    log.info(marker, "Done");
    return serialPort;
  }

//  public static SerialPort setupSerialPort(String name, String port, String matchString, Integer baudrate, SerialPortMessageListener listener) throws Exception {
//    log.info(marker, "Setting up " + name);
//    if (port == null || port.length() == 0) {
//      port = SerialDiscovery.autodiscoverPort(name, matchString);
//      log.info(marker, "Autodiscovery returned " + port);
//    }
//    if (port == null) {
//      log.warn("Failed to determine port");
//    } else {
//      try {
//        SerialPort serialPort = new SerialPort(port, baudrate, listener);
//        serialPort.connect();
//        log.info(marker, "Done");
//        return serialPort;
//      } catch (NoSuchPortException e) {
//        log.error(marker, "Failed to connect port", e);
//      }
//    }
//    return null;
//  }

  private SerialPort() {
    super();
  }

  private SerialPort(String portName, SerialPortMessageListener listener) {
    this.name = portName;
    this.listener = listener;
  }

  private SerialPort(String portName, int speed, SerialPortMessageListener listener) {
    this.name = portName;
    this.speed = speed;
    this.listener = listener;
  }

  public void connect() throws SerialException {
    int dataBits = 8;
    int stopBits = 1;
    int parity = 0;
    log.info("Connecting to port:" + name + ", speed:" + speed + ", dataBits:" + dataBits + ", stopBits:" + stopBits + ", parity:" + parity);
    try {
      CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(name);
      if (portIdentifier.isCurrentlyOwned()) {
        log.info("Error: Port is currently in use {}", portIdentifier.getCurrentOwner());
      } else {
        gnu.io.SerialPort commPort = portIdentifier.open(getClass().getName(), 2000);

        if (commPort != null) {
          port = commPort;
          port.setSerialPortParams(speed, dataBits, stopBits, parity);
          port.addEventListener(this);
          port.notifyOnDataAvailable(true);
        } else {
          log.info("Error: Only serial ports are handled by this example.");
        }
      }
    } catch (NoSuchPortException e) {
      throw new SerialException("No such serial port", e);
    } catch (PortInUseException e) {
      throw new SerialException("Serial port in use", e);
    } catch (UnsupportedCommOperationException e) {
      throw new SerialException("Unsupported serial communication", e);
    } catch (TooManyListenersException e) {
      throw new SerialException("Too many serial port listeners", e);
    }
  }

  public void serialEvent(SerialPortEvent event) {
    if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
      try {
        BufferedReader reader = new BufferedReader(new InputStreamReader(port.getInputStream()));
        String msg;
        while (reader.ready() && (msg = reader.readLine()) != null) {
          if (msg != null && msg.length() > 0) {
            listener.serialReceived(msg);
          }
        }
      } catch (IOException e) {
        log.error("Error while handling serial event", e);
      }
    } else {
      log.warn("Unhandled serial event {}", event.getEventType());
    }
  }

  public void serialReply(String message) {
    if (port == null) {
      return;
    }
    try {
      message += "\r";
      port.getOutputStream().write(message.getBytes());
    } catch (IOException e) {
      log.error("Error while writing to serial port");
    }
  }

  public void disconnect() {
    this.port.close();
  }
}
