package nl.robfaber.serial;

import lombok.val;

import java.io.IOException;

public class SerialStreamExample {
  public static void main(String[] args) throws SerialException, IOException {
    val device = SerialDevice.of("port", "/dev/tty.usbserial-0001");
    val stream = SerialPortStream.open(device, 57600);
    val in = stream.getInputStream();
    char ch;
    while ((ch = (char) in.read()) != -1) {
      System.out.print(ch);
    }
  }
}
