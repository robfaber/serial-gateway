package nl.robfaber.serial;

import gnu.io.SerialPortEvent;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SerialPortTest {

  @Mock
  private SerialPortMessageListener listener;
  @Mock
  private gnu.io.SerialPort port;

  @InjectMocks
  private SerialPort serialPort;

  @Test
  public void simpleTest() throws IOException {
    PipedOutputStream out = new PipedOutputStream();
    InputStream inputStream = new PipedInputStream(out);
    SerialPortEvent serialEvent = new SerialPortEvent(port, SerialPortEvent.DATA_AVAILABLE, true, true);
    when(port.getInputStream()).thenReturn(inputStream);
    out.write("test\n".getBytes());
    serialPort.serialEvent(serialEvent);
    verify(listener).serialReceived("test");
  }

  @Test
  public void simpleTest2() throws IOException {
    PipedOutputStream out = new PipedOutputStream();
    InputStream inputStream = new PipedInputStream(out);
    SerialPortEvent serialEvent = new SerialPortEvent(port, SerialPortEvent.DATA_AVAILABLE, true, true);
    when(port.getInputStream()).thenReturn(inputStream);
    out.write("test\nabc\n".getBytes());
    serialPort.serialEvent(serialEvent);
    verify(listener).serialReceived("test");
    verify(listener).serialReceived("abc");
  }
}
