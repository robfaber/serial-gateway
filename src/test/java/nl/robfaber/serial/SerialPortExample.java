package nl.robfaber.serial;

import lombok.extern.slf4j.Slf4j;
import lombok.val;

@Slf4j
public class SerialPortExample {
  public static void main(String[] args) throws SerialException {

    log.info("Listing ports");
    val ports = SerialDiscovery.listPorts("/dev/tty");
    ports.forEach(port -> log.info(port));

    if (args.length > 0) {
      SerialDevice device = SerialDiscovery.autodiscoverPort("Modem", args[0]);
      if (device != null) {
        log.info("Found serial device {}", device);
      } else {
        device = SerialDevice.of("Modem", args[0]);
      }
      SerialPort.open(device, 57600, log::info);
    }
  }
}
