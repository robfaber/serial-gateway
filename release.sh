#!/bin/bash -e

export VERSION=$1
export SNAPSHOT=$2
export GPG_TTY=$(tty)

if [[ -z "$VERSION" ]]; then
  echo "ERROR: Version is undefined"
  exit 1
fi
if [[ -z "$SNAPSHOT" ]]; then
  echo "ERROR: Snapshot version is undefined"
  exit 1
fi

mvn versions:set -DnewVersion=${VERSION} -DgenerateBackupPoms=false
mvn clean deploy -P release
git add . && git commit -m "Release" && git tag $VERSION && git push

mvn versions:set -DnewVersion=${SNAPSHOT} -DgenerateBackupPoms=false
git add . && git commit -m "New snapshot" && git push

