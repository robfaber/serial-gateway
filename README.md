# serial-gateway

This project contains my implementation of a serial gateway


# Raspberry Pi Installation

The dependency version we use is 2.1.7 while the below install command installs a 2.2pre2 release
To install on raspberry pi; `sudo apt-get install librxtx-java`

Make sure your program is started with the following command line parameter; `-Djava.library.path=/usr/lib/jni`
